# Rapid Front-end Projects

This repo contains multiple quick and small front-end projects that I use to build different things. Purely front-end, (almost) no frameworks, packages, etc.

To build and run each project:

1. `cd` into the project's root folder
2. use your server to serve the project; for example I use this command: `http-server . -c-1`
3. go to `localhost:8080`
