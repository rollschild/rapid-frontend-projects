const imageContainer = document.getElementById("image-container");
const loader = document.getElementById("loader");
let photosArray = [];
let ready = false;
let imagesLoaded = 0;
let totalImages = 0;

const setAttributes = (element, attributes) => {
  for (const key in attributes) {
    element.setAttribute(key, attributes[key]);
  }
};

const imageLoaded = () => {
  console.log("Image loaded");
  imagesLoaded += 1;

  if (imagesLoaded === totalImages) {
    loader.hidden = true;
    console.log("all images for this fetch have been loaded!");
    ready = true;
    imagesLoaded = 0;
  }
};

const displayPhotos = (photosArray) => {
  totalImages = photosArray ? photosArray.length : 0;
  photosArray.forEach((photo) => {
    const item = document.createElement("a");
    // item.setAttribute("href", photo.links.html);
    // item.setAttribute("target", "_blank");
    setAttributes(item, {
      href: photo.links.html,
      target: "_blank",
    });

    const img = document.createElement("img");
    // img.setAttribute("src", photo.urls.regular);
    // img.setAttribute("alt", photo.alt_description);
    // img.setAttribute("title", photo.alt_description);
    setAttributes(img, {
      src: photo.urls.regular,
      alt: photo.alt_description,
      title: photo.alt_description,
    });
    img.addEventListener("load", imageLoaded);

    item.appendChild(img);
    imageContainer.appendChild(item);
  });
};

// Unsplash API
const count = 10;
const accessKey = "Nb8hwU87RviVh_ZRG43hNwbRKnfE748-ukTHVjIbBiw";
const apiUrl = `https://api.unsplash.com/photos/random?client_id=${accessKey}&count=${count}`;

const getPhotos = async () => {
  try {
    const response = await fetch(apiUrl);
    photosArray = await response.json();
    displayPhotos(photosArray);
  } catch (error) {
    console.error("ERROR:", error);
  }
};

// infinite scroll
window.addEventListener("scroll", () => {
  if (
    window.scrollY + window.innerHeight >= document.body.scrollHeight &&
    ready
  ) {
    console.log("at the bottom of the page");
    ready = false;
    getPhotos();
  }
});

getPhotos();
