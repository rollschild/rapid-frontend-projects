const listArrayMap = {
  "backlog-list": [],
  "progress-list": [],
  "complete-list": [],
  "on-hold-list": [],
};
let draggedItem;
let dragging;
let currentHoveredColumn;
let originParentId;
let destinationParentId;

const addBtns = document.querySelectorAll(".add-btn:not(.solid)");
const addBtnsColumnMap = Object.fromEntries(
  Array.from(addBtns).map((ele) => [
    ele.id.slice(0, ele.id.indexOf("-add-btn")),
    ele,
  ]),
);
(function () {
  Object.keys(addBtnsColumnMap).forEach((key) => {
    addBtnsColumnMap[key].addEventListener("click", showInputBox(key));
  });
})();

const saveItemBtns = document.querySelectorAll(".solid");
const saveItemBtnsColumnMap = Object.fromEntries(
  Array.from(saveItemBtns).map((ele) => [
    ele.id.slice(0, ele.id.indexOf("-save-btn")),
    ele,
  ]),
);
(function () {
  Object.keys(saveItemBtnsColumnMap).forEach((key) => {
    saveItemBtnsColumnMap[key].addEventListener("click", hideInputBox(key));
  });
})();

const addItemContainers = document.querySelectorAll(".add-container");
const addItemContainersMap = Object.fromEntries(
  Array.from(addItemContainers).map((ele) => [
    ele.id.slice(0, ele.id.indexOf("-add-container")),
    ele,
  ]),
);

// textarea
const addItems = document.querySelectorAll(".add-item");
const addItemsMap = Object.fromEntries(
  Array.from(addItems).map((ele) => [
    ele.id.slice(0, ele.id.indexOf("-add-item")),
    ele,
  ]),
);

// Item lists
const dragItemList = document.querySelectorAll(".drag-item-list");
const dragItemListMap = Object.fromEntries(
  Array.from(dragItemList).map((ele) => [ele.id, ele]),
);

const backlogList = document.getElementById("backlog-list");
backlogList.addEventListener("dragover", allowDrop);
backlogList.addEventListener("drop", drop);
backlogList.addEventListener("dragenter", dragEnter("backlog-list"));

const progressList = document.getElementById("progress-list");
progressList.addEventListener("dragover", allowDrop);
progressList.addEventListener("drop", drop);
progressList.addEventListener("dragenter", dragEnter("progress-list"));

const completeList = document.getElementById("complete-list");
completeList.addEventListener("dragover", allowDrop);
completeList.addEventListener("drop", drop);
completeList.addEventListener("dragenter", dragEnter("complete-list"));

const onHoldList = document.getElementById("on-hold-list");
onHoldList.addEventListener("dragover", allowDrop);
onHoldList.addEventListener("drop", drop);
onHoldList.addEventListener("dragenter", dragEnter("on-hold-list"));

let updatedOnLoad = false;

function showInputBox(columnName) {
  return (e) => {
    addBtnsColumnMap[columnName].style.visibility = "hidden";
    saveItemBtnsColumnMap[columnName].style.display = "flex";
    addItemContainersMap[columnName].style.display = "flex";
  };
}
function hideInputBox(columnName) {
  return (e) => {
    addBtnsColumnMap[columnName].style.visibility = "visible";
    saveItemBtnsColumnMap[columnName].style.display = "none";
    addItemContainersMap[columnName].style.display = "none";

    addTextToColumn(columnName);
  };
}

function addTextToColumn(columnName) {
  const text = addItemsMap[columnName].value;
  listArrayMap[columnName].push(text);
  updateDOM();
  resetTextInput(columnName);
}

function resetTextInput(columnName) {
  addItemsMap[columnName].value = "";
}

const getSavedColumns = () => {
  if (localStorage.getItem("backlog-list")) {
    Object.keys(listArrayMap).forEach(
      (key) => (listArrayMap[key] = JSON.parse(localStorage[key])),
    );
  } else {
    listArrayMap["backlog-list"] = ["backlong item 1"];
    listArrayMap["progress-list"] = ["progress item 1"];
    listArrayMap["complete-list"] = ["complete item 1"];
    listArrayMap["on-hold-list"] = ["onHold item 1"];
  }
};

const updateSavedColumns = () => {
  Object.entries(listArrayMap).forEach(([itemName, itemArray]) => {
    localStorage.setItem(itemName, JSON.stringify(itemArray));
  });
};

function drag(e) {
  draggedItem = e.target;
  const originalParentColumn = draggedItem.closest(".drag-item-list");
  originParentId = originalParentColumn.id;
}
function allowDrop(e) {
  e.preventDefault();
}
function drop(e) {
  e.preventDefault();

  if (!currentHoveredColumn) {
    return;
  }
  // remove background color/padding
  Object.values(dragItemListMap).forEach((column) =>
    column.classList.remove("over"),
  );

  // add item to the column
  const parent = dragItemListMap[currentHoveredColumn];
  destinationParentId = parent.id;
  parent.appendChild(draggedItem);

  rebuildColumn();
  resetDrag();
}

function resetDrag() {
  draggedItem = undefined;
  currentHoveredColumn = undefined;
  originParentId = "";
  destinationParentId = "";
}

function dragEnter(columnName) {
  return (e) => {
    const hoveredItem = e.target;
    const hoveredColumnId = hoveredItem.classList.contains("drag-item")
      ? hoveredItem.closest(".drag-item-list").id
      : hoveredItem.classList.contains("drag-item-list")
      ? hoveredItem.id
      : "";

    // if dragged within its original column, do nothing
    if (hoveredColumnId === originParentId) {
      return;
    }

    dragItemListMap[columnName].classList.add("over");
    currentHoveredColumn = columnName;
  };
}

const createItemElement = (columnElement, item, index) => {
  const listElement = document.createElement("li");
  listElement.classList.add("drag-item");
  listElement.textContent = item;
  listElement.draggable = true;

  // clicking on the item will turn it into a text box
  listElement.id = index;

  listElement.addEventListener("mousedown", (e) => {
    dragging = false;
  });
  listElement.addEventListener("mousemove", () => {
    dragging = true;
  });
  listElement.addEventListener("mouseup", (e) => {
    if (dragging) {
      return drag;
    }
    listElement.contentEditable = true;
    listElement.focus();
  });
  listElement.addEventListener("dragstart", drag);
  listElement.addEventListener("focusout", updateItem(columnElement, index));

  columnElement.appendChild(listElement);
};

function updateItem(columnElement, index) {
  return (_) => {
    // delete the item if it's blank
    // or update the item in the list

    const updatedText = Array.from(columnElement.children)[index].textContent;

    if (!updatedText) {
      // delete it
      listArrayMap[columnElement.id].splice(index, 1);
    } else {
      listArrayMap[columnElement.id].splice(index, 1, updatedText);
    }

    columnElement.contentEditable = false;
    updateDOM();
  };
}

const updateDOM = () => {
  if (!updatedOnLoad) {
    getSavedColumns();
    updatedOnLoad = true;
  }

  backlogList.textContent = "";
  listArrayMap["backlog-list"].forEach((backlogItem, index) =>
    createItemElement(backlogList, backlogItem, index),
  );

  progressList.textContent = "";
  listArrayMap["progress-list"].forEach((progressItem, index) =>
    createItemElement(progressList, progressItem, index),
  );

  completeList.textContent = "";
  listArrayMap["complete-list"].forEach((completeItem, index) =>
    createItemElement(completeList, completeItem, index),
  );

  onHoldList.textContent = "";
  listArrayMap["on-hold-list"].forEach((onHoldItem, index) =>
    createItemElement(onHoldList, onHoldItem, index),
  );

  updateSavedColumns();
};

function rebuildColumn() {
  const originList = document.getElementById(originParentId);
  const destinationList = document.getElementById(destinationParentId);

  listArrayMap[originParentId] = Array.from(originList.children).map(
    (li) => li.textContent,
  );
  listArrayMap[destinationParentId] = Array.from(destinationList.children).map(
    (li) => li.textContent,
  );

  // this may not be needed at all though
  updateDOM();
}

// On Load
updateDOM();
